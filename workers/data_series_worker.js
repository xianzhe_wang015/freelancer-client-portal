function getData(m) {
	if (m) {
		var dataSeriesRequest = new XMLHttpRequest();

		dataSeriesRequest.onreadystatechange = function() {
			if (dataSeriesRequest.readyState == XMLHttpRequest.DONE) {
				postMessage(dataSeriesRequest.responseText);
			}
		};

		dataSeriesRequest.open('GET', '/api/site/now?mprn=' + m, true);
		dataSeriesRequest.send();
	} else {
		postMessage(false);
	}

}

onmessage = function(e) {
	var m = e.data.mprn;
	getData(m);

	setInterval(function() {
		if (m) {
			var dataSeriesRequest = new XMLHttpRequest();

			dataSeriesRequest.onreadystatechange = function() {
				if (dataSeriesRequest.readyState == XMLHttpRequest.DONE) {
					postMessage(dataSeriesRequest.responseText);
				}
			};

			dataSeriesRequest.open('GET', '/api/site/now?mprn=' + m, true);
			dataSeriesRequest.send();
		} else {
			postMessage(false);
		}

	}, 1000);
};
