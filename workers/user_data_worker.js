onmessage = function(e) {
	var userDataRequest = new XMLHttpRequest();

	userDataRequest.onreadystatechange = function() {
		if (userDataRequest.readyState == XMLHttpRequest.DONE) {
			postMessage(userDataRequest.responseText);
		}
	};

	userDataRequest.open('GET', '/user/info', true);
	userDataRequest.send();

};
