onmessage = function(e) {
	var userNodeRequest = new XMLHttpRequest();

	userNodeRequest.onreadystatechange = function() {
		if (userNodeRequest.readyState == XMLHttpRequest.DONE) {
			postMessage(userNodeRequest.responseText);
		}
	};

	userNodeRequest.open('GET', '/user/meter', true);
	userNodeRequest.send();

};
